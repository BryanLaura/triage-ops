# frozen_string_literal: true

require_relative '../lib/dast_helper'

Gitlab::Triage::Resource::Context.include DastHelper
