# GitLab triage operations

*This is a prototype. The goal is to add immediate value to the GitLab Engineering function, while determining if it adds value to customers. If so, we will work with Product Management to [bring this functionality to GitLab the product](https://gitlab.com/groups/gitlab-org/-/epics/636).*

Triage operations for GitLab issues and merge requests, with two strategies: reactive & scheduled operations.

## The bot

We're using [@gitlab-bot](https://gitlab.com/gitlab-bot) as the user to run
triage operations. The credentials could be found in the shared 1Password
vault.

The same bot is also used in the following projects:

* https://gitlab.com/gitlab-org/release-tools
* https://gitlab.com/gitlab-org/async-retrospectives
* https://gitlab.com/gitlab-com/gl-infra/triage-ops

## Reactive operations

This project runs a service which listens to webhooks from [gitlab-org group](https://gitlab.com/gitlab-org),
and it reacts upon webhooks events. So far it runs:

* AvailabilityPriority (when label added)
  * This ensures availability issues have minimal priorities based on severity.
    For example, for `~severity::1` and `~severity::2` availability issues, the minimal
    priority is `~priority::1`.
* BackstageLabel (when label added)
  * This hints and removes `~"backstage [DEPRECATED]"` label because we don't
    want to use this label anymore.
* CustomerLabel (when comment added)
  * This adds a `~customer` label whenever the comment contains a link to a
    customer support ticket link.
* TypeLabel (when label added)
  * This adds a type label whenever a subtype label is added.

### Running as a local server

* In the project directory, build the docker image. This creates a docker image named `triage-ops`:
```
docker build --file=Dockerfile.rack -t triage-ops .
```

* Run the server locally (here, in dry mode) using Docker, passing required environment variables.

```
docker run -itp 8080:8080 \
  -e GITLAB_API_ENDPOINT=http://localhost:3000/api/v4 \
  -e GITLAB_WEBHOOK_TOKEN=gitlab_webhook_token \
  -e GITLAB_API_TOKEN=gitlab_api_token \
  -e SLACK_WEBHOOK_URL=https://example.org \
  -e DRY_RUN=1 \
  triage-ops
```

* Test the endpoint

```
curl -X POST 0.0.0.0:8080 -H "Content-Type: application/json" -H "X-Gitlab-Token: gitlab_webhook_token" -d @spec/fixture/gitlab_test_note.json
```

### Running with a local GitLab instance

You can run `triage-ops` with a local GitLab instance (e.g GDK) and have it react to events coming from the local instance.

There are some prerequisites you would need in the local GitLab instance:
1. The following groups and subgroups:
    - `gitlab-org`
    - `gitlab-com`
    - `gitlab-org/gitlab-core-team/community-members`
1. The following projects:
    - `gitlab-org/gitlab`
1. A `bot` user who is a member of `gitlab-org` and `gitlab-com` groups, with an api scoped access token. This access token should be set in the environment `GITLAB_API_TOKEN` when running `triage-ops`.
1. Enable a group webhook for the `gitlab-org` and `gitlab-com` groups that points to `http://0.0.0.0:8080`
  - Set a value for the `Secret token` field of the webhook. This secret should be set in the environment `GITLAB_WEBHOOK_TOKEN` when running `triage-ops`.
1. Enable outbound webhook request to `localhost`. See [documentation](https://docs.gitlab.com/ee/security/webhooks.html) for detail.

Once the prerequisites are all set, you can start the server locally as shown in the [Running as a local server](#running-as-a-local-server) section, but not in dry-run mode this time:

```
docker run -itp 8080:8080 \
  -e GITLAB_API_ENDPOINT=http://localhost:3000/api/v4 \
  -e GITLAB_WEBHOOK_TOKEN=gitlab_webhook_token \
  -e GITLAB_API_TOKEN=gitlab_api_token \
  -e SLACK_WEBHOOK_URL=https://example.org \
  triage-ops
```

TODO:
- [ ] Stub Slack webhook URL

### Implementing a new Processor

To implement a new processor that responds to a specific event:
1. Create a new subclass of `Processor` in `triage/processor/`
1. Implement the following methods:
    1. `#applicable?` - a method that returns `true` or `false` on whether the event is applicable to this processor.
    1. `#process` - a method that processes the event and performs what is needed.
1. Add the new processor to the `DEFAULT_PROCESSORS` in `triage/triage/handler.rb`

### Production

#### How we run on Kubernetes

The Kubernetes cluster is in the project
[gitlab-qa-resources](https://console.cloud.google.com/kubernetes/list?project=gitlab-qa-resources).

At the moment, we're not running on Serverless, but an ordinary web server to
process the webhooks.

0. <https://triage-ops.gitlab.com/> Resolve [DNS records](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/14764) to IP pointing to the `ingress-nginx-controller` Service:

       kubectl describe service ingress-nginx-controller --namespace=ingress-nginx

0. Then where the traffic goes?
0. Then traffic goes to `triage-web-ingress` since it has the `kubernetes.io/ingress.class: "nginx"` annotation

       kubectl describe ingress -l app=triage-web --all-namespaces

0. Then traffic goes to `triage-web-service` thanks to `rules[0].http.paths[0].backend.serviceName: triage-web-service`

       kubectl describe service -l app=triage-web --all-namespaces

Note: Both `triage-web-ingress` and `triage-web-service` are part of `triage-web-deployment`:

       kubectl describe deployment -l app=triage-web --all-namespaces

#### `event.from_gitlab_org?` as feature flags

We can use `event.from_gitlab_org?` to block events from
[`gitlab-org`](https://gitlab.com/gitlab-org), and deploy to production,
and then test in
[triage-test](https://gitlab.com/gl-quality/eng-prod/triage-test).
This way, we can try out the new feature in a separate project before
applying it to `gitlab-org`.

After enough tests, we can then create a merge request and remove the block,
actually applying to `gitlab-org`.

#### Setting up `kubectl` for production access

* Install `gcloud`

      brew cask install google-cloud-sdk

* Install `kubectl`

      brew install kubernetes-cli

* Log in to GCK

      gcloud auth login

* Configure `kubectl`

      gcloud container clusters get-credentials triage-ops-prod --zone us-central1 --project gitlab-qa-resources

* Run local proxy

      kubectl proxy

## Scheduled operations

This is powered by <https://gitlab.com/gitlab-org/gitlab-triage>.

Schedules are managed automatically by the `schedules-sync` job which runs for
every non-scheduled `master` commit. A `dry-run:schedules-sync` job also runs on
any non-`master` pipeline.

These jobs run the `bin/manage_schedules` script, which allows to sync pipeline
schedules and their variables.

Additionally, it will automatically disable all the pipeline schedules that
aren't declared.

Schedules are defined in the `pipeline-schedules.yml` file. Its format is as follows:

```yaml
---
gitlab-org:
  base:
    cron: '0 4 * * 1-5'
    variables:
      TRIAGE_SOURCE_TYPE: groups
      TRIAGE_SOURCE_PATH: 9970
  variants:
    - id: 11219
      ref: 'feature'
      cron: '0 4 * * 1-7'
      cron_timezone: 'Europe'
      active: true
      variables:
        TRIAGE_LABEL_COMMUNITY_CONTRIBUTIONS: 1
    - active: false
      description: 'gitlab-org weekly schedule'
      cron: '0 0 * * 1'
      variables:
        TRIAGE_TEAM_SUMMARY: 1
```

1. `base` are shared attributes and variables for all the variants of a schedule.
   - Supported attributes are `ref`, `cron`, `cron_timezone`, `active`.
1. `variants` is an array of variants for which you can define/override attributes
    and variables (e.g. useful to define a daily and a weekly job for the same group/project)
   - Supported attributes are the ones from `base`, and additionally `id` and `description`.
   - Defining an `id` allows to update an existing pipeline schedule `description`,
     otherwise it would be impossible since pipeline schedules are matched based on
     their description if no `id` is provided.
1. If `description` is omitted, it will use a default description of
   `[MANAGED] <schedule key> (<variables list>)`, e.g. for the first variant in
   the above example, that would be `[MANAGED] 'gitlab-org' ('TRIAGE_LABEL_COMMUNITY_CONTRIBUTIONS=1')`.
1. Other attributes also have a default value:
   - `ref='master'`
   - `cron_timezone='UTC'`
   - `cron='0 0 * * 1-5'` (Monday to Friday at midnight)
   - `active=true`
1. To temporarily disable a pipeline schedule, just set `active: false`.

## One-off policies

We sometimes need to run one-off policies usually after some label updates.

Following are a few guidelines for these one-off policies:

1. These policies should be created in the `policies/one-off/` folder.
1. Dry-run and actual run jobs should be added to the `.gitlab/ci/one-off.yml` file.
1. For convenience, the `TRIAGE_POLICY_FILE` variable will automatically be set based on the job name.
   For instance, the `sample-one-off-migration:dry-run` and `sample-one-off-migration` jobs
   would have `TRIAGE_POLICY_FILE="policies/one-off/sample-one-off-migration.yml"` set automatically.
1. Merge requests for one-off policies should have the ~"one-off" label set and should be closed once the policies have been run.

## Technical documentation

This section contains some guidance on how to do some current steps to test and setup new triage pipelines.

### Testing with a dry-run

When creating or changing a policy, you are able to test it by leveraging the pipelines within your merge request.

#### Preconditions

1. If you are changing or creating a new policy, be sure to open your merge request first as WIP.
1. Ensure there is data available for your test condition.

#### Steps

1. Open the `dry-run:custom` job in your merge request pipeline.
1. Enter at least the `TRIAGE_POLICY_FILE` variable with the path of the policy file you want to test, for instance:
   - Fill the `Key` field with `TRIAGE_POLICY_FILE`.
   - Fill the `Value` field with `policies/stages/hygiene/label-missed-slo.yml`.
1. By default the dry-run will run against the `gitlab-org/gitlab` project. If you want to change that, you can add:
   - A `TRIAGE_SOURCE_TYPE` variable with the value `projects` (default) or `groups`.
   - A `TRIAGE_SOURCE_PATH` variable with the path or ID of the project or group that you want to test the new policy against.
1. Click "Trigger this manual action" and enjoy! 🍿

### Setting up a new pipeline schedule

Creating a scheduled pipeline is done programmatically, via a merge request and can be done at the same time a new policy is introduced (or not).

Refer to the `The schedules` section above for more details.

#### Testing the schedule with a dry-run pipeline

Similar to testing with a dry-run for a policy file, we can also trigger a
pipeline using the same variables configured in the schedule.

1. Open the `dry-run:schedule` job in your merge request pipeline.
1. Enter `TRIAGE_SCHEDULE_NAME` variable with the path of schedule you want to test, for instance:
   - Fill the `Key` field with `TRIAGE_SCHEDULE_NAME`.
   - Fill the `Value` field with `gitlab-org/build/cng`
1. (Optional) If there are multiple schedules with the same name, we can also
   pass `TRIAGE_SCHEDULE_INDEX` to indicate which one we want to run.
   By default, it's the first one, which the index is `0`.
1. (Optional) If we want to add some additional variables to the triggered
   pipeline, we can prefix those variables with `BYPASSING_` so they will be
   passed along to the triggered pipeline without the prefix. For example,
   suppose we want to add `TRIAGE_FAKE_TODAY_FOR_MISSED_RESOURCES=2020-01-01`
   to the triggered pipeline, we can:
   - Fill the `Key` field with `BYPASSING_TRIAGE_FAKE_TODAY_FOR_MISSED_RESOURCES`.
   - Fill the `Value` field with `2020-01-01`
1. This will use the same variables defined as in the schedule, along with `DRY_RUN=1` to make sure everything should be running in dry-run mode.
1. Click "Trigger this manual action" and you can see the triggered pipeline
  URL printed in the job log. You can also go to the same pipeline page and
  check the triggered downstream pipeline.

### Generating policy files and CI jobs

Due to some [technical limitation](https://gitlab.com/gitlab-org/gitlab-triage/-/issues/191),
it's much easier to generate policy files and CI jobs via scripts.

We once [tried to generate a child pipeline](https://gitlab.com/gitlab-org/quality/triage-ops/-/merge_requests/483#note_337672436)
to run the generated CI jobs, but it turned out a child pipeline cannot
generate another child pipeline, yet. Thus it's much easier if we just
commit them into the repository.

Suppose we want to generate the CI jobs and policies for *Collate merge
requests requiring attention*, we can run below:

```shell
bin/generate_group_policies -t policies/template/group/merge-requests-needing-attention.yml.erb
bin/generate_ci_jobs -t .gitlab/ci/template/merge-requests-needing-attention.yml.erb
```

We have more templates than just that though. In order to regenerate
everything, we can run:

```shell
find policies/template/group -type f | xargs -n1 bundle exec bin/generate_group_policies -t
bundle exec bin/generate_stage_policies -t policies/template/stage/bug-release-post-triage.yml.erb --only plan,create --assign backend_engineering_manager,frontend_engineering_manager
find .gitlab/ci/template -type f | xargs -n1 bundle exec bin/generate_ci_jobs -t
```

There is a `regeneration-check` job to verify if generated files are all
up-to-date. The job will run the same to iterate through all the templates,
and check if it's identical to what were committed.

## Diagnosing executed jobs and pipelines

After a scheduled pipeline has been executed, the link to the latest pipeline for that schedule can be found in the [pipeline schedules list](https://gitlab.com/gitlab-org/quality/triage-ops/-/pipeline_schedules). This can be a good way to find the latest instance of a particular pipeline or job associated that executes a particular rule.

For generated triage reports, they often contain a URL to the job that created them. This can be useful to find out more about the number of issues discovered by the rule's query or to determine at what time of day a rule was executed.

## Development

### Install gems

```
bundle install
```

### Run tests

You can run the test suite with `bundle exec rspec`.

Guard is also supported (see the [`Guardfile`](Guardfile)). Install Guard with `gem install guard`, then start it with `guard`.
