# frozen_string_literal: true

require 'gitlab'
require 'net/http'

class StaleBugs
  COM_GITLAB_API = 'https://gitlab.com/api/v4'
  MAX_WAIT_TIME = 3600
  GITLAB_BOT_USERNAME = 'gitlab-bot'

  Gitlab.configure do |config|
    config.endpoint = COM_GITLAB_API
  end

  def self.stale?(project_path:, token:, project_id:, issue_iid:, days:, debug: ENV['DEBUG'])
    (new(project_path: project_path, token: token, project_id: project_id, issue_iid: issue_iid, days: days, debug: debug)
    .send :days_since_last_human_update) > days
  end

  def self.active?(project_path:, token:, project_id:, issue_iid:, days:, debug: ENV['DEBUG'])
    (new(project_path: project_path, token: token, project_id: project_id, issue_iid: issue_iid, days: days, debug: debug)
    .send :days_since_last_human_update) <= days
  end

  def initialize(project_path:, token:, project_id:, issue_iid:, days:, debug: ENV['DEBUG'])
    raise ArgumentError, 'An API token is needed!' if token.nil?

    @project_path = project_path
    @project_id = project_id
    @issue_iid = issue_iid
    @days = days
    @token = token
    @debug = debug
    @client = Gitlab.client(private_token: token)
  end

  private

  def days_since_last_human_update
    age = -1
    notes = client.issue_notes(project_id, issue_iid, { order_by: "updated_at", sort: "desc" })
    notes.each do |note|
      if note['author']['username'] != GITLAB_BOT_USERNAME
        age = (Date.today - Date.parse(note['updated_at'])).to_i
        break
      end
    end

    if age == -1 # If there are no comments at all, take the issue_created_date
      issue = client.issue(project_id, issue_iid)
      age = (Date.today - Date.parse(issue['created_at'])).to_i
    end

    age
  end

  attr_reader :project_path, :token, :client, :project_id, :issue_iid
end
