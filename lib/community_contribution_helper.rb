# frozen_string_literal: true

module CommunityContributionHelper
  # https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html#project-bot-users
  PROJECT_SERVICE_ACCOUNT_REGEX = %r{\Aproject_(\d+)_bot(\d*)\z}.freeze

  GITLAB_DEPENDENCY_UPDATE_BOT = 'gitlab-dependency-update-bot'

  def bot_author?
    automation_bot? || project_bot?
  end

  private

  def automation_bot?
    author == GITLAB_DEPENDENCY_UPDATE_BOT
  end

  def project_bot?
    author.match?(PROJECT_SERVICE_ACCOUNT_REGEX)
  end
end
