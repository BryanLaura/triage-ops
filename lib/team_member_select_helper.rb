# frozen_string_literal: true

module TeamMemberSelectHelper

  private

  def select_random_team_member(department, specialty = nil, role = nil)
    WwwGitLabCom.team_from_www.each_with_object([]) do |(username, data), memo|
      memo << "@#{username}" if user_matches?(data, department, specialty, role)
    end.sample
  end

  def user_matches?(data, department, specialty, role)
    results = []
    results << (data['departments']&.any? { |dept| dept == department }) if department
    results << (data['specialty']&.include? specialty) if specialty
    results << (data['role'].include? role) if role

    results.all? true
  end

end
