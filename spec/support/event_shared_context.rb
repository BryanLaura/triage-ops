require_relative '../../triage/triage/user'

RSpec.shared_context 'with event' do |event_class = 'Triage::Event'|
  let(:event_attrs) { {} }
  let(:label_names) { [] }
  let(:added_label_names) { [] }
  let(:user_username) { 'root' }
  let(:event) do
    instance_double(event_class, {
      object_kind: 'issue',
      action: 'open',
      user: { 'id' => 1, 'username' => user_username },
      user_username: user_username,
      author_id: 42,
      resource_author: Triage::User.new(id: 42, username: 'joe'),
      key: 'issue.open',
      noteable_path: '/foo',
      label_names: label_names,
      added_label_names: added_label_names,
      resource_open?: true,
      payload: {}
    }.merge(event_attrs))
  end
end
