# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../triage/rate_limit'

require 'digest'
require 'slack-messenger'

module Triage
  class CommandRequestReview < Processor
    include RateLimit

    SLACK_CHANNEL = '#mr-coaching'
    SLACK_ICON = ':robot_face:'
    SLACK_MESSAGE_TEMPLATE = <<~MESSAGE
      Hi MR coaches, a contributor has requested a review in %<comment_url>s.
    MESSAGE

    react_to 'merge_request.note'
    define_command name: 'request_review'

    def initialize(event, messenger: slack_messenger)
      super(event)
      @messenger = messenger
    end

    def applicable?
      event.from_gitlab_org? &&
        event.by_noteable_author? &&
        command.valid?(event)
    end

    def process
      send_review_request
      send_reply_to_author
    end

    private

    attr_reader :messenger

    def cache_key
      @cache_key ||= Digest::MD5.hexdigest("request_review-commands-sent-#{event.user['id']}-#{event.noteable_path}")
    end

    def rate_limit_count
      1
    end

    def rate_limit_period
      86400 # 1 day
    end

    def send_review_request
      message = format(SLACK_MESSAGE_TEMPLATE, comment_url: event.url)
      messenger.ping(message)
    end

    def slack_messenger
      options = {
        channel: SLACK_CHANNEL,
        username: GITLAB_BOT,
        icon_emoji: SLACK_ICON
      }

      Slack::Messenger.new(ENV['SLACK_WEBHOOK_URL'], options)
    end

    def send_reply_to_author
      add_comment <<~MARKDOWN.chomp
        :wave: @#{event.user_username},

        Thanks for requesting a review. I've notified [the merge request coaches](https://about.gitlab.com/company/team/?department=merge-request-coach).

        They will get back to you as soon as they can.

        If you have not received any response, you may ask for a review again after 1 day.
      MARKDOWN
    end
  end
end
