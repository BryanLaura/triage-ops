# frozen_string_literal: true

require_relative '../triage/processor'

module Triage
  class JiHuContribution < Processor
    LABEL_NAME = 'JiHu contribution'

    react_to 'merge_request.open'

    def applicable?
      event.jihu_contributor?
    end

    def process
      add_discussion(%Q{/label ~"#{LABEL_NAME}"\ncc @gitlab-com/gl-security/appsec this is a ~"#{LABEL_NAME}", please follow the [JiHu contribution review process](https://about.gitlab.com/handbook/ceo/chief-of-staff-team/jihu-support/jihu-security-review-process.html#security-review-workflow-for-jihu-contributions)})
    end
  end
end
