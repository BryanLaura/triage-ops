# frozen_string_literal: true

source "https://rubygems.org"

git_source(:github) { |repo_name| "https://github.com/#{repo_name}" }

gem "activesupport", "~> 5.2"
gem "sentry-raven", require: false
gem "gitlab-triage", "~> 1.20"
gem "gitlab", "~> 4.16.1", require: false

# Picked because it's used in gitlab. Can use any which follows redirect
gem "httparty"

# Reactive
gem "mini_cache", "~> 1.1.0", require: false
gem "sucker_punch", "~> 2.0", require: false
gem "rack", require: false
gem "puma", require: false
gem "slack-messenger", require: false

group :development do
  gem "guard"
  gem "guard-rspec"
end

group :test do
  gem "rspec", require: false
  gem "rspec-parameterized", require: false
  gem "timecop", require: false
  gem "webmock", require: false
end

group :development, :test, :danger do
  gem 'gitlab-dangerfiles', '~> 2.7.1', require: false
end
